<?php

declare(strict_types=1);

use C33s\Bundle\EntityLoaderBundle\EntityLoader\BaseContent;

final class Content20210715Example extends BaseContent
{
    public static function createdAtDate(): DateTimeImmutable
    {
        return new DateTimeImmutable('2021-07-09T13:56:39.6982405Z');
    }

    public function preventLoadBeforeDate(): DateTimeInterface
    {
        return new DateTimeImmutable('2021-06-01');
    }

    public function preventLoadAfterDate(): DateTimeInterface
    {
        return new DateTimeImmutable('2022-01-01');
    }

    public function getEntities(): array
    {
        $entities[] = new NewsEntry('My Headline', 'my content', '2021-07-15');
        $entities[] = new NewsEntry('Other Headline', 'my other content', '2021-07-17');

        return $entities;
    }
}
