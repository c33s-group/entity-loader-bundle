# Entity Loader Bundle

A mixture of [`doctrine/doctrine-fixtures-bundle`][doctrine_fixtures] and
[`doctrine/doctrine-migrations-bundle`][doctrine_migrations] to store content in
php files and only load new content while preserving existing content. kind of
[`doctrine/doctrine-migrations-bundle`][doctrine_migrations] with access to the
entity manager.

## Usage

create a php file in `src/DataContent` (you also can use the
[`c33s/maker-extra-bundle`][maker_extra] to automate this process), add your
content as entities and run `php bin/console content:load` in your symfony
project to load the content.
```php
final class Content20210715Example extends BaseContent
{
    public static function createdAtDate(): DateTimeImmutable
    {
        return new DateTimeImmutable('2021-07-09T13:56:39.6982405Z');
    }

    public function getEntities(): array
    {
        $entities[] = new NewsEntry('My Headline', 'my content', '2021-07-15');
        $entities[] = new NewsEntry('Other Headline', 'my other content', '2021-07-17');

        return $entities;
    }
}
```

loaded content files are stored in a tables and won't load again. you can also
implement the `preventLoadBeforeDate` and/or `preventLoadAfterDate` to prevent
load before or after a specific date:

```php
final class Content20210715Example extends BaseContent
{
    //...
    public function preventLoadBeforeDate(): DateTimeInterface
    {
        return new DateTimeImmutable('2021-06-01');
    }

    public function preventLoadAfterDate(): DateTimeInterface
    {
        return new DateTimeImmutable('2022-01-01');
    }
    //...
}
```
via cli options you can also set a global "prevent load before/after" for all
content files, which can be used together with `current-date` to "simulate" the
current date.

you can also implement `shouldFlushBefore` and `shouldFlushAfter` to force a
entity manager flush before/after this content file.
```php
final class Content20210715Example extends BaseContent
{
    //...
    public function shouldFlushBefore(): bool
    {
        return false;
    }

    public function shouldFlushAfter(): bool
    {
        return false;
    }
    //...
}
```

the content loader fires an event before `PreLoadEvent` and after
`PostLoadEvent` all loading is done.

## Why

Build from the need to add/create content via fixtures but allow to only
add content and not remove or rebuild existing content, to allow the application
to add new content by itself (e.g. adding a new article will auto create a
news entry).

doctrine/fixtures could have been used, but the risk of loosing data by
forgetting to add `--append` was too high. doctrine/migrations could have been
used but the clear separation between data and migration could have been
difficult. additional the usage of the entity manager in migrations is
[discouraged][em_in_mig].

[em_in_mig]: https://stackoverflow.com/questions/19502630/use-entitymanager-in-migrations-file
[doctrine_fixtures]: https://github.com/doctrine/DoctrineFixturesBundle
[doctrine_migrations]: https://github.com/doctrine/DoctrineMigrationsBundle
[maker_extra]:
