<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle\Entity;

use C33s\Bundle\EntityLoaderBundle\EntityLoader\ContentInterface;
use C33s\Bundle\EntityLoaderBundle\Repository\LoadedContentInformationRepository;
use C33s\Doctrine\Entity\Traits\Field;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LoadedContentInformationRepository::class)
 * @ORM\Table(name="c33s_loaded_content_info")
 */
class LoadedContentInformation
{
    use Field\RequiresUuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $version;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeInterface $executedAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="integer")
     */
    private int $loadedEntityCount;

    public static function createFromClass(ContentInterface $object, int $entityCount): self
    {
        return new self(
            get_class($object),
            new DateTimeImmutable(),
            $object::createdAtDate(),
            $entityCount
        );
    }

    public function __construct(
        string $version,
        DateTimeInterface $executedAt,
        DateTimeInterface $createdAt,
        int $entityCount
    ) {
        $this->version = $version;
        $this->createdAt = $createdAt;
        $this->executedAt = $executedAt;
        $this->loadedEntityCount = $entityCount;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function getExecutedAt(): DateTimeInterface
    {
        return $this->executedAt;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getLoadedEntityCount(): int
    {
        return $this->loadedEntityCount;
    }
}
