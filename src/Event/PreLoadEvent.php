<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle\Event;

use C33s\Bundle\EntityLoaderBundle\EntityLoader\ContentInterface;
use Symfony\Contracts\EventDispatcher\Event;

final class PreLoadEvent extends Event
{
    /**
     * @var ContentInterface[]
     */
    private array $contents;

    /**
     * @param ContentInterface[] $contents
     */
    public function __construct(array $contents)
    {
        $this->contents = $contents;
    }

    /**
     * @return ContentInterface[]
     */
    public function getContents(): array
    {
        return $this->contents;
    }
}
