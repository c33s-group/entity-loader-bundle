<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class C33sEntityLoaderBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
