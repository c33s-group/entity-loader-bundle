<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle\Repository;

use C33s\Bundle\EntityLoaderBundle\Entity\LoadedContentInformation;
use C33s\Bundle\EntityLoaderBundle\EntityLoader\ContentInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LoadedContentInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method LoadedContentInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method LoadedContentInformation[]    findAll()
 * @method LoadedContentInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoadedContentInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LoadedContentInformation::class);
    }

    public function has(ContentInterface $content): bool
    {
        $result = $this->createQueryBuilder('l')
            ->andWhere('l.version = :version')
            ->setParameter('version', get_class($content))
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return !(null === $result);
    }
}
