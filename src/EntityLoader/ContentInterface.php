<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle\EntityLoader;

use DateTimeInterface;

interface ContentInterface
{
    /**
     * Requires to be static as this is a requirement of
     * Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait.
     */
    public static function getDefaultPriority(): int;

    /**
     * @return object[]
     */
    public function getEntities(): array;

    /**
     * The default implementation of getDefaultPriority uses this method to do an ordered loading, so this method also
     * has to be static.
     */
    public static function createdAtDate(): DateTimeInterface;

    public function preventLoadBeforeDate(): DateTimeInterface;

    public function preventLoadAfterDate(): DateTimeInterface;

    public function shouldFlushBefore(): bool;

    public function shouldFlushAfter(): bool;
}
