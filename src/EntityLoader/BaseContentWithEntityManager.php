<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle\EntityLoader;

use Doctrine\ORM\EntityManagerInterface;

abstract class BaseContentWithEntityManager extends BaseContent
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }
}
