<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle\EntityLoader;

use DateTimeImmutable;
use DateTimeInterface;

abstract class BaseContent implements ContentInterface
{
    public static function getDefaultPriority(): int
    {
        return static::createdAtDate()->getTimestamp();
    }

    /**
     * Default: Always load by defining a date which is always before "now". Won't load if "now" is change  with the
     * current-date option.
     */
    public function preventLoadBeforeDate(): DateTimeInterface
    {
        return new DateTimeImmutable('now -2 days');
//        return new DateTimeImmutable(static::createdAtDate()); //load content after its creation date
    }

    /**
     * Default: always load by defining a date which is always after "now". Won't load if "now" is change  with the
     * current-date option.
     */
    public function preventLoadAfterDate(): DateTimeInterface
    {
        return new DateTimeImmutable('now +2 days');
    }

    /**
     * Default: false (do not flush before load).
     */
    public function shouldFlushBefore(): bool
    {
        return false;
    }

    /**
     * Default: false (do not flush after load).
     */
    public function shouldFlushAfter(): bool
    {
        return false;
    }
}
